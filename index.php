<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
setlocale(LC_CTYPE, 'fr_FR.UTF-8');

include_once 'class/Post.class.php';

$resoH = Post::myPOST("resoh");
$resoV = Post::myPOST("resov");
$nbrImg = Post::myPOST("nbrimg");
$dim = Post::myPOST("dim");
$nbBit = Post::myPOST("nbBit");

$result = ($resoH * $resoV * $nbrImg * $nbBit * $dim)/1000000;

//var_dump($result);
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Calcul video</title>
    </head>
    <body>
        <form class="form-horizontal" method='post' action='index.php'>
            <fieldset>

                <!-- Form Name -->
                <legend>Calcul de débit vidéo</legend>

                <!-- Select Basic -->
<!--                <div class="form-group">
                    <label class="col-md-4 control-label" for="selectbasic">Résolution H x V</label>
                    <div class="col-md-4">
                        <select id="selectbasic" name="selectbasic" class="form-control">
                            <option value="1">720 x 576</option>
                            <option value="2">1280 x 720</option>
                            <option value="3">1920 x 1080</option>
                            <option value="4">3840 x 2160</option>
                        </select>
                    </div>
                </div>-->

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" 
                           for="resoh">Résolution Horizontale</label>  
                    <div class="col-md-4">
                        <input id="resoh" name="resoh" placeholder="" 
                               class="form-control input-md" type="number" 
                               required="" value="<?php echo $resoH ?>">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" 
                           for="resov">Résolution Verticale</label>  
                    <div class="col-md-4">
                        <input id="resov" name="resov" placeholder="" 
                               class="form-control input-md" type="number" 
                               required="" value="<?php echo $resoV ?>">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" 
                           for="nbrimg">Nombres d'images / s</label>  
                    <div class="col-md-4">
                        <input id="nbrimg" name="nbrimg" placeholder=""
                               class="form-control input-md" type="number" 
                               required="" value="<?php echo $nbrImg ?>">

                    </div>
                </div>

                <!-- Multiple Radios (inline) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="dim"></label>
                    <div class="col-md-4"> 
                        <label class="radio-inline" for="dim-0">
                            <input name="dim" id="dim-0" value="3" 
                                   checked="checked" type="radio">
                            4:4:4
                        </label> 
                        <label class="radio-inline" for="dim-1">
                            <input name="dim" id="dim-1" value="2" type="radio">
                            4:2:2
                        </label> 
                        <label class="radio-inline" for="dim-2">
                            <input name="dim" id="dim-2" value="1.5" type="radio">
                            4:2:0
                        </label> 
                        <label class="radio-inline" for="dim-3">
                            <input name="dim" id="dim-3" value="1.5" type="radio">
                            4:1:1
                        </label> 
                        <label class="radio-inline" for="dim-4">
                            <input name="dim" id="dim-4" value="1.25" type="radio">
                            3:1:1
                        </label> 
                        <label class="radio-inline" for="dim-5">
                            <input name="dim" id="dim-5" value="1.5" type="radio">
                            3:1.5:1.5
                        </label> 
                        <label class="radio-inline" for="dim-6">
                            <input name="dim" id="dim-6" value="1.125" type="radio">
                            3:1.5:0
                        </label> 
                    </div>
                </div>

                <!-- Multiple Radios (inline) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nbBit"></label>
                    <div class="col-md-4"> 
                        <label class="radio-inline" for="nbBit-0">
                            <input name="nbBit" id="nbBit-0" value="8" 
                                   checked="checked" type="radio">
                            8 bits
                        </label> 
                        <label class="radio-inline" for="nbBit-1">
                            <input name="nbBit" id="nbBit-1" value="10" type="radio">
                            10 bits
                        </label> 
                        <label class="radio-inline" for="nbBit-2">
                            <input name="nbBit" id="nbBit-2" value="12" type="radio">
                            12 bits
                        </label>
                        <label class="radio-inline" for="nbBit-3">
                            <input name="nbBit" id="nbBit-3" value="14" type="radio">
                            14 bits
                        </label> 
                        <label class="radio-inline" for="nbBit-4">
                            <input name="nbBit" id="nbBit-4" value="16" type="radio">
                            16 bits
                        </label>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="calcul"></label>
                    <div class="col-md-4">
                        <button id="calcul" name="calcul" 
                                class="btn btn-primary">Calculer</button>
                    </div>
                </div>

            </fieldset>
        </form>

        <h2>Résultats du calcul :</h2>
        <div>
            Débit vidéo <strong><?php echo round($result, 0) ?> en Mb/s</strong> ou 
            <strong><?php echo round($result/1000, 2) ?> en Gb/s</strong> <br>
            Débit vidéo <strong><?php echo round($result/8, 0) ?> en Mio/s</strong> ou 
            <strong><?php echo  round($result/1000/8, 2) ?> en Gio/s</strong> <br>
        </div>

    </body>
</html>
